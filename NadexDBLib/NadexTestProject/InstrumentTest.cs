﻿using NadexDBLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NadexTestProject
{
    
    
    /// <summary>
    ///This is a test class for InstrumentTest and is intended
    ///to contain all InstrumentTest Unit Tests
    ///</summary>
    [TestClass()]
    public class InstrumentTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Instrument Constructor
        ///</summary>
        [TestMethod()]
        public void InstrumentConstructorTest()
        {
            Instrument target = new Instrument();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CreateInstrument
        ///</summary>
        [TestMethod()]
        public void CreateInstrumentTest()
        {
            int instrument_id = 0; // TODO: Initialize to an appropriate value
            string instrument_name = string.Empty; // TODO: Initialize to an appropriate value
            int instrument_type_id = 0; // TODO: Initialize to an appropriate value
            Instrument expected = null; // TODO: Initialize to an appropriate value
            Instrument actual;
            actual = Instrument.CreateInstrument(instrument_id, instrument_name, instrument_type_id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for instrument_id
        ///</summary>
        [TestMethod()]
        public void instrument_idTest()
        {
            Instrument target = new Instrument(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.instrument_id = expected;
            actual = target.instrument_id;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for instrument_name
        ///</summary>
        [TestMethod()]
        public void instrument_nameTest()
        {
            Instrument target = new Instrument(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.instrument_name = expected;
            actual = target.instrument_name;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for instrument_price
        ///</summary>
        [TestMethod()]
        public void instrument_priceTest()
        {
            Instrument target = new Instrument(); // TODO: Initialize to an appropriate value
            Nullable<Decimal> expected = new Nullable<Decimal>(); // TODO: Initialize to an appropriate value
            Nullable<Decimal> actual;
            target.instrument_price = expected;
            actual = target.instrument_price;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for instrument_type_id
        ///</summary>
        [TestMethod()]
        public void instrument_type_idTest()
        {
            Instrument target = new Instrument(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.instrument_type_id = expected;
            actual = target.instrument_type_id;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for updated_on
        ///</summary>
        [TestMethod()]
        public void updated_onTest()
        {
            Instrument target = new Instrument(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> expected = new Nullable<DateTime>(); // TODO: Initialize to an appropriate value
            Nullable<DateTime> actual;
            target.updated_on = expected;
            actual = target.updated_on;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
