﻿using NadexDBLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.EntityClient;
using System.Data.Objects;

namespace NadexTestProject
{
    
    
    /// <summary>
    ///This is a test class for stockDBEntitiesTest and is intended
    ///to contain all stockDBEntitiesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class stockDBEntitiesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for stockDBEntities Constructor
        ///</summary>
        [TestMethod()]
        public void stockDBEntitiesConstructorTest()
        {
            EntityConnection connection = null; // TODO: Initialize to an appropriate value
            stockDBEntities target = new stockDBEntities(connection);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for stockDBEntities Constructor
        ///</summary>
        [TestMethod()]
        public void stockDBEntitiesConstructorTest1()
        {
            string connectionString = string.Empty; // TODO: Initialize to an appropriate value
            stockDBEntities target = new stockDBEntities(connectionString);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for stockDBEntities Constructor
        ///</summary>
        [TestMethod()]
        public void stockDBEntitiesConstructorTest2()
        {
            stockDBEntities target = new stockDBEntities();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddToInstrument
        ///</summary>
        [TestMethod()]
        public void AddToInstrumentTest()
        {
            stockDBEntities target = new stockDBEntities(); // TODO: Initialize to an appropriate value
            Instrument instrument = null; // TODO: Initialize to an appropriate value
            target.AddToInstrument(instrument);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Instrument
        ///</summary>
        [TestMethod()]
        public void InstrumentTest()
        {
            stockDBEntities target = new stockDBEntities(); // TODO: Initialize to an appropriate value
            ObjectSet<Instrument> actual;
            actual = target.Instrument;
            Assert.Inconclusive("Verify the correctness of this test method.");

        }
    }
}
