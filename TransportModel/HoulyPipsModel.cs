﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportModel
{
    public class HoulyPipsModel
    {
        private HoulyPipsPkModel _pkModel;
        public HoulyPipsPkModel hourlyPipsPK
        {
            get 
            {
                if (_pkModel == null)
                {
                    _pkModel = new HoulyPipsPkModel();
                }
                return _pkModel; 
            }
            set 
            {
                _pkModel = value; 
            }
        }

        public long? pip { get; set; }
        public int hourlyInterval { get; set; }
        public long? lastDate { get; set; }
    }
}
