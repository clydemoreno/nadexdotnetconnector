﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication
{
    public class DataSource
    {

        public string  Name { get; set; }
        public string Test { get; set; }

        public List<DataSource> GetSource()
        {

            List<DataSource> list = new List<DataSource> 
            { 
            new DataSource { Name = "name01" , Test="test01" },
            new DataSource { Name = "name02" , Test="test02" }
            };

            return list;

        }

    }
}
