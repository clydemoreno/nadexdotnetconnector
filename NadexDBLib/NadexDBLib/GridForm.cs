﻿using NadexDBLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TransportModel;

namespace WindowsFormsApplication
{
    public partial class GridForm : Form
    {
        public GridForm()
        {
            InitializeComponent();

            //Scheduler s = Scheduler.Instantiate;
            //s.Settings["Name"] = "EUR/JPY";
            //s.Settings["Price"] = 888.88;
            //s.Settings["MovingAverageInDays"] = "15";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Scheduler s = Scheduler.Instantiate;
            WorkTask task = new WorkTaskGetHourlyPips();
            //task.Settings["Name"] = "EUR/JPY";
            //task.Settings["Price"] = 888.88;
            //task.Settings["MovingAverageInDays"] = "15";
            task.Settings = s.Settings;

            task.Start();

            object returnObj = task.returnObject;
            List<HoulyPipsModel> list = (List<HoulyPipsModel>)returnObj;

            List<HoulyPipsPkModel> pkList = new List<HoulyPipsPkModel>();
            //string, string, string, string, long, int, DateTime

            var newList = (from h in list
                        select new
                        {
                            InstrumentName = h.hourlyPipsPK.instrumentName,
                            EndHour = h.hourlyPipsPK.endHour,
                            StartHour = h.hourlyPipsPK.startHour,
                            MovingAverageInDays = h.hourlyPipsPK.movingAverageInDays,
                            Pip = h.pip,
                            HourlyInterval = h.hourlyInterval,
                            LastDate = JavaMSConverter(h.lastDate)
                        }).ToList();

            //foreach (var i in list)
            //{
            //    HoulyPipsPkModel pk = new HoulyPipsPkModel
            //    {
            //     instrumentName = i.hourlyPipsPK.instrumentName,
            //     endHour = i.hourlyPipsPK.endHour,
            //     startHour = i.hourlyPipsPK.startHour,
            //     movingAverageInDays = i.hourlyPipsPK.movingAverageInDays
            //    };
            //    pkList.Add(pk);
                
            //}

            var bs = new BindingSource();
            bs.DataSource = newList;
            dataGridView1.DataSource = bs;
        }

        private void btnGetHourlyPips_Click(object sender, EventArgs e)
        {
            Scheduler s = Scheduler.Instantiate;
            WorkTask task = new WorkTaskGetHourlyPips();
            //task.Settings["Name"] = "EUR/JPY";
            //task.Settings["Price"] = 888.88;
            //task.Settings["MovingAverageInDays"] = "15";
            task.Settings = s.Settings;

            task.Start();

            object returnObj = task.returnObject;
            List<HoulyPipsModel> list = (List<HoulyPipsModel>)returnObj;

            List<HoulyPipsPkModel> pkList = new List<HoulyPipsPkModel>();
            //string, string, string, string, long, int, DateTime
            var newList = (from h in list
                           select new
                           {
                               InstrumentName = h.hourlyPipsPK.instrumentName,
                               EndHour = h.hourlyPipsPK.endHour,
                               StartHour = h.hourlyPipsPK.startHour,
                               MovingAverageInDays = h.hourlyPipsPK.movingAverageInDays,
                               Pip = h.pip,
                               HourlyInterval = h.hourlyInterval,
                               LastDate = JavaMSConverter(h.lastDate)
                           }).ToList();

            var bs = new BindingSource();
            bs.DataSource = newList;
            dataGridView1.DataSource = bs;
        }

        private static DateTime JavaMSConverter(long? javaMS)
        {
            DateTime UTCBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dt = UTCBaseTime.Add(new TimeSpan(long.Parse(javaMS.ToString()) * TimeSpan.TicksPerMillisecond)).ToLocalTime();

            return dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


    }
}
