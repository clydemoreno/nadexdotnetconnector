﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.Model;
using System.Net;
using System.IO;
using TransportModel;

namespace DBAccess
{
    public class Controller
    {
        public void DeleteNews()
        {
            using (stockDBEntities context = new stockDBEntities())
            {
                List<News> list = null;
                IQueryable<News> news = from p in context.News select p;
                list = news.ToList<News>();
                foreach (News items in list)
                {
                    context.News.Remove(items);
                    
                }
                context.SaveChanges();
                //context.News.SqlQuery("Delete * From News");
                //context.SaveChanges();
            }
        }

        public void DownloadNews(string urlLink)
        {
            try
            {
                DeleteNews();
                using (stockDBEntities context = new stockDBEntities())
                {
                    WebResponse response = null;
                    Stream remoteStream = null;
                    WebRequest request = WebRequest.Create(urlLink);
                    News news = new News();

                    if (request != null)
                    {
                        response = request.GetResponse();
                        if (response != null)
                        {
                            remoteStream = response.GetResponseStream();
                            StreamReader datalist = new StreamReader(remoteStream);
                            string[] line = datalist.ReadLine().Split(',');
                            while (line != null)
                            {
                                try
                                {                                  
                                    string[] day = line[0].Split(' ');
                                    string time = line[1];
                                    string importance = line[5];
                                    string currency = line[3].ToUpper();
                                    NadexCurrency[] list = GetNadex(currency);
                                    if (!String.IsNullOrWhiteSpace(time) && !importance.StartsWith("L") && list != null)
                                    {
                                        DateTime gmtTime = Convert.ToDateTime(string.Format("{0} {1}", day[1], day[2])).Add(Convert.ToDateTime(time).TimeOfDay);
                                        DateTime easternTime = gmtTime.AddHours(-4.00);
                                        foreach (NadexCurrency nadex in list)
                                        {
                                            news.currency = nadex.name;
                                            news.date = easternTime;
                                            news.events = line[4];
                                            news.importance = line[5];
                                            news.actual = line[6];
                                            news.forecast = line[7];
                                            news.previous = line[8];
                                            news.updated_date = DateTime.Now;
                                            context.News.Add(news);
                                            context.SaveChanges();
                                        }
                                    }
                                    line = datalist.ReadLine().Split(',');
                                }
                                catch
                                {
                                    line = datalist.ReadLine().Split(',');
                                }
                                
                            }
                        }

                    }

                }
            }
            catch 
            {
            }
        }

        public NadexCurrency[] GetNadex(string currency)
        {
            try
            {
                List<NadexCurrency> list = null;
                //NadexCurrency list = null;
                using (stockDBEntities context = new stockDBEntities())
                {
                    IQueryable<NadexCurrency> nadex = from c in context.NadexCurrencies
                                        where c.name.Contains(currency)
                                        select c;
                    list = nadex.ToList<NadexCurrency>();
                }
                return list.ToArray<NadexCurrency>();
            }
            catch
            {
                return null;
            }
        }

        private void ConvertNewsToNewsInfo(NewsInfo newsinfo, News news)
        {
            newsinfo.Date = news.date.GetValueOrDefault();
            newsinfo.Currency = news.currency;
            newsinfo.Events = news.events;
            newsinfo.Importance = news.importance;
            newsinfo.Actual = news.actual;
            newsinfo.Forecast = news.forecast;
            newsinfo.Previous = news.previous;
            newsinfo.NewsID = news.news_id;
            newsinfo.UpdatedDate = news.updated_date;
        }
    }
}
