﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportModel
{
    public class HoulyPipsPkModel
    {
        public string instrumentName { get; set; }
        public string startHour { get; set; }
        public string endHour { get; set; }
        public string movingAverageInDays { get; set; }
    }
}
