﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsFormsApplication;

namespace NadexDBLib
{
    public  class AccessForm
    {
        #region Singleton : HourlyPipsForm
        private AccessForm() { }
        private static AccessForm instance;
        public static AccessForm Instantiate
        {
            get
            {
                if (instance == null)
                {
                    instance = new AccessForm();
                }
                return instance;
            }
        }
        #endregion

        public void Open()
        {
            GridForm gf = new GridForm();
            gf.Show();
        }

    }
}
