﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.ComponentModel;
using TransportModel;
using NadexDBLib;


namespace TestConsoleApplication
{
    public class Program
    {
        public static int counter = 0;

        static void Main(string[] args)
        {
            TestEmailAlert();
            //TestWorkTasks();
            //TestForm();
            Console.Read();
        }

        static void TestForm()
        {
            AccessForm hp = AccessForm.Instantiate;

            hp.Open();
        }

        static void TestWorkTasks()
        {
            Scheduler s = Scheduler.Instantiate;
            Print("Instantiated");

            //s.AssemblyName = "NadexDBLib.dll";

            //s.WorkTask.Add(new WorkTaskUpdateInstrument()); //not working

            //s.WorkTask.Add(new WorkTaskGetCurrentNews());

            s.WorkTask.Add(new WorkTaskGetHourlyPips());

            //s.WorkTask.Add(new WorkTaskGetAllNews()); //same code with WorkTaskStoreAllNews.cs
            //s.WorkTask.Add(new WorkTaskStoreAllNews()); //same code with WorkTaskGetAllNews.cs

            s.Settings["Name"] = "EUR/JPY";
            s.Settings["Price"] = 888.88;
            s.Settings["MovingAverageInDays"] = "15";

            s.StartWorker(1); //set scheduler every 1 min
            //s.StartWorker();
            s.PropertyChanged += s_PropertyChanged;
        }

        static void TestEmailAlert()
        {
            Scheduler s = Scheduler.Instantiate;
            Print("Instantiated");

            //s.AssemblyName = "NadexDBLib.dll";

            //s.WorkTask.Add(new WorkTaskUpdateInstrument()); //not working

            //s.WorkTask.Add(new WorkTaskGetCurrentNews());

            s.WorkTask.Add(new WorkTaskGetEmailAlert());

            //s.WorkTask.Add(new WorkTaskGetHourlyPips());

            //s.WorkTask.Add(new WorkTaskGetAllNews()); //same code with WorkTaskStoreAllNews.cs
            //s.WorkTask.Add(new WorkTaskStoreAllNews()); //same code with WorkTaskGetAllNews.cs

            s.Settings["instrumentName"] = "GBP/USD[Daily]";
 
            s.StartWorker(1); //set scheduler every 1 min
            //s.StartWorker();
            s.PropertyChanged += s_PropertyChanged;
        }


        static void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlePropertyChange(sender, e);
        }

        private static void HandlePropertyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;
                
                foreach (var item in lists)
                {

                    #region *** STORE ALL NEWS ***

                    if (item.GetType() == typeof(List<NewsModel>))
                    {
                        List<NewsModel> newsList = (List<NewsModel>)(item);

                        Scheduler s = Scheduler.Instantiate;
                        NewsCollection nc = NewsCollection.Instantiate;

                        //ALL NEWS
                        //Hashtable allNews = nc.GetAllNewsHashTable(newsList, 10);

                        #region FILTERED NEWS

                        Hashtable filteredNews = nc.GetFilteredHashTable("EUR/USD", newsList, 10);

                        foreach (DictionaryEntry i in filteredNews)
                        {
                            NewsHashValueModel value = (NewsHashValueModel)i.Value;
                            Print("KEY ||  " + i.Key.ToString());
                        }
                        Print(filteredNews.Keys.Count.ToString());

                        #endregion
                    }
                    #endregion

                    #region *** GET ALL NEWS ***
                    if (item.GetType() == typeof(List<NewsModel>))
                    {
                        List<NewsModel> newsList = (List<NewsModel>)(item);
                        foreach (NewsModel n in newsList)
                        {
                            Print(string.Format("Id--{0} | importance--{1} | currency--{2}", n.newsId, n.importance, n.currency));
                        }
                    }
                    #endregion

                    #region *** CURRENT NEWS ***

                    if (item.GetType() == typeof(NewsModel))
                    {
                        NewsModel news = (NewsModel)(item);
                        //IsOkToTrade = (news.newsId != null);	
                        Print(string.Format("ALL NEWS | Id--{0} | importance--{1} | currency--{2}", news.newsId, news.importance, news.currency));
                    }
                    #endregion

                    #region *** PRICE ***

                    if (item.GetType() == typeof(List<string>))
                    {
                        List<string> priceStatus = (List<string>)(item);
                        Print(string.Format("price status : \t{0}", priceStatus.FirstOrDefault().ToString()));
                    }
                    #endregion

                    #region *** HOURLY PIPS *** 

                    if (item.GetType() == typeof(List<HoulyPipsModel>))
                    {
                        List<HoulyPipsModel> list = (List<HoulyPipsModel>)(item);

                        foreach (HoulyPipsModel h in list.OrderByDescending(o => o.hourlyInterval))
                        {
                            Print(string.Format("Hour/Pips   : {0}\t{1}" , h.hourlyPipsPK.instrumentName, h.hourlyInterval, h.lastDate));
                        }
                    }
                    #endregion

                    //Print(string.Format("\t\t counter : {0}", counter));   
                }
            }

            counter++;
        }

        static void Print(string s)
        {
            Console.WriteLine(s);
        }




    }
}
