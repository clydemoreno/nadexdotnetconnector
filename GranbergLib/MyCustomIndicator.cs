#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using System.Collections;
using NadexDBLib;
using TransportModel;

using System.Collections.Generic;
#endregion

namespace NinjaTrader.Indicator
{
    public class MyCustomIndicator : IndicatorBase
    {
        [Description("This Class is from GranbergLib.dll")]

        #region COPY THESE

        #region Variables
        private int myInput0 = 1; // Default setting for MyInput0
			public static int counter = 0;
        #endregion
		
		private bool isOkToTrade = false;
		
		protected override void OnStartUp()
		{
            Print("OnStartUp MyCustomIndicator.cs     " + DateTime.Now.ToLongTimeString());

			Scheduler s = Scheduler.Instantiate;

			s.WorkTask.Add(new WorkTaskGetAllNews());
            s.WorkTask.Add(new WorkTaskGetCurrentNews());

            s.Settings["Name"] = "EUR/USD";  //Instrument.FullName.ToString();
            s.Settings["Price"] = 888.88;
            s.Settings["MovingAverageInDays"] = "15";

            s.StartWorker(1);//every 1 minute
            s.PropertyChanged += s_PropertyChanged; 
		}
		
		public void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlePropertyChange(sender, e);
			Print(string.Format("count: {0}\tIsOkToTrade: {1}",counter ,IsOkToTrade));
			counter++;
        }
		
 		private void HandlePropertyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;

                foreach (var item in lists)
                {
					/*CURRENT NEWS*/
                    if (item.GetType() == typeof(NewsModel))
                    {
                        NewsModel news = (NewsModel)(item);
						isOkToTrade = (news.newsId != null);	
                        Print(string.Format("newsId : {0}", news.newsId));
					}
					
					/*GET ALL NEWS*/
					#region GET ALL NEWS
//                    if (item.GetType() == typeof(List<NewsInfo>))
//                    {
//                        List<NewsInfo> list = (List<NewsInfo>)(item);
//                        foreach (NewsInfo n in list)
//                        {
//                            Print(string.Format("News   : {0}\t{1}", n.events, n.importance));
//                        }
//                    }
					#endregion
					
					/*UPDATE PRICE*/
					#region UPDATE PRICE
//                    if (item.GetType() == typeof(List<string>))
//                    {
//                        List<string> priceStatus = (List<string>)(item);
//                        Print(string.Format("price status : \t{0}", priceStatus.FirstOrDefault().ToString()));
//                    }
					#endregion
					
                }
            }
        }
		
		
        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.Orange), PlotStyle.Line, "Plot0"));
            Overlay				= false;
        }

        protected override void OnBarUpdate()
        {
            Plot0.Set(Close[0]);
        }

        #endregion

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int MyInput0
        {
            get { return myInput0; }
            set { myInput0 = Math.Max(1, value); }
        }
		
        public bool IsOkToTrade
        {
            get 
			{ 
				Update();
				return isOkToTrade; 
			}
            //set { isOkToTrade = value; }
        }
		
        public BoolSeries MyBoolean{get;set;}
		

        #endregion
    }
}
