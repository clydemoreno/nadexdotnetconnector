﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TransportModel;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INadexJsonService" in both code and config file together.
[ServiceContract]
public interface INadexJsonService
{
	[OperationContract]
	void DoWork();
    [OperationContract]
    [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
    string login(String username, String password);
    [OperationContract]
    [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
    List<NewsInfo> GetNewsInfo();
}
