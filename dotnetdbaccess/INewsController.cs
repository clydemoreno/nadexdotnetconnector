﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransportModel;

namespace DBAccess
{
    public interface INewsController
    {
        NewsInfo[] GetNews();
        List<NewsInfo> GetNewsInfo();
    }
}
