﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TransportModel;

namespace NadexDBLib
{
    public class WorkTaskUpdateInstrument : WorkTask
    {
        public WorkTaskUpdateInstrument()
        {
            Url = "http://tradingws.kyoobiq.com:8080/NadexWS/webresources/instrument/updateInstrument";
        }

        public override void Start()
        {
            InstrumentPriceRequest i = new InstrumentPriceRequest
            {
                instrumentName = Settings["Name"] != null ? (String)Settings["Name"] : string.Empty,
                instrumentPrice = Settings["Price"] != null ? (double)Settings["Price"] : 0
            };

            //InstrumentPriceRequest i = new InstrumentPriceRequest
            //{
            //    instrumentName = "EUR/USD",
            //    instrumentPrice = 555.55
            //};

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(Url); request.KeepAlive = false;
            request.Method = "POST";

            string json = JsonConvert.SerializeObject(i);
            byte[] postBytes = Encoding.UTF8.GetBytes(json);

            request.ContentType = "application/json; charset=UTF-8";

            Stream requestStream = request.GetRequestStream();

            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string jsonResponse;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                jsonResponse = rdr.ReadToEnd();
            }

            DeserializeJson(jsonResponse);

            Console.WriteLine("Price Successfully Updated...");
        }

        public override void DeserializeJson(string jsonResponse)
        {
            returnObject = new List<string> { "Price Updated! > WorkTaskUpdateInstrument.cs > at > " + DateTime.Now };
        }

        
    }
}
