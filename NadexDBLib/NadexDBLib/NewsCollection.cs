﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransportModel;

namespace NadexDBLib
{
    public class NewsCollection
    {
        
        #region Singleton : Scheduler
        private NewsCollection() 
        { 
        }
        private static NewsCollection instance;
        public static NewsCollection Instantiate
        {
            get
            {
                if (instance == null)
                {
                    instance = new NewsCollection();
                }
                return instance;
            }
        }
        #endregion

        private object _HashKey { get; set; }
        private NewsHashValueModel _HashValue;

        //ADD NEWS TO HASHTABLE

        public Hashtable GetAllNewsHashTable(List<NewsModel> newsList, int endTimeMinuteGap)
        {
            Hashtable hashTable = new Hashtable();
            foreach (NewsModel news in newsList)
            {
                _HashValue = SetValue(news, endTimeMinuteGap);
                _HashKey = SetKey(news);

                if (!hashTable.ContainsKey(_HashKey))
                {
                    hashTable.Add(_HashKey, _HashValue);
                }
            }

            return hashTable;
        }

        public Hashtable GetFilteredHashTable(string instrumentName, List<NewsModel> newsList, int endTimeMinuteGap)
        {
            Hashtable hashTable = new Hashtable();
            foreach (NewsModel news in newsList)
            {
                
                _HashValue = SetValue(news, endTimeMinuteGap);
                _HashKey = SetKey(news);

                if ((!hashTable.ContainsKey(_HashKey)) && (instrumentName == news.currency))
                {
                    hashTable.Add(_HashKey, _HashValue);
                }
            }

            return hashTable;
        }

        private NewsHashValueModel SetValue(NewsModel n, int endTimeMinuteGap)
        {
            DateTime newsDate = JavaMSConverter(n.date);
            NewsHashValueModel filledValue = new NewsHashValueModel 
                            { 
                                News = n,
                                StartTime = newsDate,
                                EndTime = newsDate.AddMinutes(endTimeMinuteGap)
                            };

            return filledValue;
        }

        private object SetKey(NewsModel n)
        {
            DateTime d = JavaMSConverter(n.date);

            string k = string.Format("{0}|{1}|{2}|{3}|{4}", n.currency, d.ToShortDateString(), d.Hour, d.Minute,n.importance);
            return k;
        }

        public List<NewsHashValueModel> GetHashTableValueOrderByDate(Hashtable hashTable)
        {
            List<NewsHashValueModel> newsList = new List<NewsHashValueModel>();
            List<NewsHashValueModel> orderedNewsList = new List<NewsHashValueModel>();

            foreach (DictionaryEntry entry in hashTable)
            {
                NewsHashValueModel value = (NewsHashValueModel)entry.Value;
                newsList.Add(value);
            }

            if (newsList.Count == hashTable.Keys.Count)
            {
                foreach (NewsHashValueModel news in newsList.OrderBy(n => n.StartTime))
                {
                    orderedNewsList.Add(news);
                }
            }

            return orderedNewsList;
        }

        private static DateTime JavaMSConverter(long? javaMS)
        {
            DateTime UTCBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dt = UTCBaseTime.Add(new TimeSpan(long.Parse(javaMS.ToString()) * TimeSpan.TicksPerMillisecond)).ToLocalTime();

            return dt;
        }
    }



}
