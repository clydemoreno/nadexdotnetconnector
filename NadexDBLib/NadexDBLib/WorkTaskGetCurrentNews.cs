﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TransportModel;

namespace NadexDBLib
{
    public class WorkTaskGetCurrentNews : WorkTask
    {
        public WorkTaskGetCurrentNews()
        {
            Url = "http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news/findCurrentNews";
        }

        public override void Start()
        {
            InstrumentModel i = new InstrumentModel
            {
                instrumentName = Settings["Name"] != null ?  (String)Settings["Name"] : string.Empty
            };

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(Url); request.KeepAlive = false;
            request.Method = "POST";

            string json = JsonConvert.SerializeObject(i);
            byte[] postBytes = Encoding.UTF8.GetBytes(json);

            request.ContentType = "application/json; charset=UTF-8";

            Stream requestStream = request.GetRequestStream();

            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string jsonResponse;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                jsonResponse = rdr.ReadToEnd();
            }

            DeserializeJson(jsonResponse);
            
        }

        public override void DeserializeJson(string jsonResponse)
        {
            try
            {
                returnObject = JsonConvert.DeserializeObject<NewsModel>(jsonResponse);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }


    }
}
