﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TransportModel;

namespace NadexDBLib
{
    public class WorkTaskGetHourlyPips : WorkTask
    {
        public WorkTaskGetHourlyPips()
        {
            Url = "http://tradingws.kyoobiq.com:8080/NadexWS/webresources/instrument/getHourlyPips";
        }

        public override void Start()
        {
            HoulyPipsPkModel pk = new HoulyPipsPkModel
            {
                instrumentName = Settings["Name"] != null ? (String)Settings["Name"] : string.Empty,
                startHour = "00",
                endHour = "04",
                movingAverageInDays = Settings["MovingAverageInDays"] != null ? (String)Settings["MovingAverageInDays"] : string.Empty
            };

            //HoulyPipsPkModel pk = new HoulyPipsPkModel
            //{
            //    instrumentName = "EUR/JPY",
            //    startHour = "00",
            //    endHour = "04",
            //    movingAverageInDays = "15"
            //};

            HoulyPipsModel i = new HoulyPipsModel { pip = 3166, lastDate = 0, hourlyInterval = 4, hourlyPipsPK = pk };

            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(Url); request.KeepAlive = false;
            request.Method = "POST";

            string json = JsonConvert.SerializeObject(i);

            byte[] postBytes = Encoding.UTF8.GetBytes(json);

            request.ContentType = "application/json; charset=UTF-8";

            Stream requestStream = request.GetRequestStream();

            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string jsonResponse;
                using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
                {
                    jsonResponse = rdr.ReadToEnd();
                }

                DeserializeJson(jsonResponse);
        }

        public override void DeserializeJson(string jsonResponse)
        {
            try
            {
                returnObject = JsonConvert.DeserializeObject<List<HoulyPipsModel>>(jsonResponse);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }
    }
}
