﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportModel
{
    public class NewsModel
    {
        public long? date
        {
            get;
            set;
        }
        //private DateTime date;

        //public DateTime Date
        //{
        //    get { return date; }
        //    set { date = value; }
        //}

        //private string currency;

        public string currency
        {
            get;
            set;
        }

        //private string events;
        public string events
        {
            get;
            set;
        }

        //private string importance;
        public string importance
        {
            get;
            set;
        }

        //private string actual;
        public string actual
        {
            get;
            set;
        }

        //private string forecast;
        public string forecast
        {
            get;
            set;
        }

        //private string previous;
        public string previous
        {
            get;
            set;
        }

        //private int newsID;
        public int? newsId
        {
            get;
            set;
        }

        //private DateTime updatedDate;=
        public long? updatedDate
        {
            get;
            set;
        }
    }
}
