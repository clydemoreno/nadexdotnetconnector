﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using TransportModel;


namespace NadexDBLib
{
    public class Scheduler : INotifyPropertyChanged
    {
        #region Singleton : Scheduler
        private Scheduler()
        {
        }
        private static Scheduler instance;
        public static Scheduler Instantiate
        {
            get
            {
                lock (new object())
                {
                    if (instance == null)
                    {
                        instance = new Scheduler();
                    }
                }
                return instance;
            }
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        public double Price { get; set; }
        public string AverageType { get; set; }

        private Dictionary<string, object> settings;
        public Dictionary<string, object> Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Dictionary<string, object>();
                }
                return settings;
            }
        }

        public string AssemblyName { get; set; }

        private ArrayList resultList;
        public ArrayList ResultList
        {
            get
            {
                if (resultList == null)
                {
                    resultList = new ArrayList();
                }
                return resultList;
            }
        }

        private List<WorkTask> workTask;
        public List<WorkTask> WorkTask
        {
            get
            {
                if (workTask == null)
                {
                    workTask = new List<WorkTask>();
                }
                return workTask;
            }
        }

        #endregion

        public void StartWorker(int minuteInterval)
        {
            Timer.Enabled = true;
            Timer.Interval = (1000) * (10) * minuteInterval;
        }
        public void StartWorker()
        {
                if (!Worker.IsBusy)
                {
                    Worker.RunWorkerAsync();
                }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName, object sender)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(sender, new PropertyChangedEventArgs(propertyName));
            }
        }

        private Timer timer;
        public Timer Timer
        {
            get
            {
                if (timer == null)
                {
                    timer = new Timer();
                    timer.Elapsed += timer_Elapsed;
                }
                return timer;
            }

        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!Worker.IsBusy)
            {
                Worker.RunWorkerAsync();
            }
        }

        #region Lazy Loading : Background Worker
        private BackgroundWorker worker;
        public BackgroundWorker Worker
        {
            get
            {
                if (worker == null)
                {
                    worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;
                    worker.WorkerSupportsCancellation = true;
                    worker.DoWork += new DoWorkEventHandler(bw_DoWork);
                    worker.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
                    worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
                }
                return worker;
            }

        }
        #endregion

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            Print("Scheduler.cs > DoWork > at >  " + DateTime.Now.ToLongTimeString());

            foreach (var item in WorkTask)
            {
                //item.Parameters.Add(Name);//Instrument Name for (GetCurrentNews && Updating Price)
                //item.Parameters.Add(Price);//Instrument Price for (Updating Price)
                //item.Parameters.Add(AverageType);
                item.Settings = Settings;
                item.Start();
                ResultList.Add(item.returnObject);
            }
            e.Result = ResultList;

        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                Print(string.Format("***\t\t RunWorkerCompleted > Cancelled > at > {0}", DateTime.Now));
            }

            else if (!(e.Error == null))
            {
                Print(string.Format("***\t\t RunWorkerCompleted > Error > {0} > at > {1}" ,e.Error.Message , DateTime.Now));
            }

            else
            {
               Print(string.Format("***\t\t RunWorkerCompleted > Done at > {0}" , DateTime.Now));
               OnPropertyChanged("ResultList", e.Result);
            }
        }
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine(e.ProgressPercentage.ToString() + "%");
        }

        private void Print(string s)
        {
            Console.WriteLine(s);
        }

    }
}
