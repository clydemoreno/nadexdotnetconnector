﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportModel
{
    public class InstrumentPriceRequest
    {
        public string instrumentName { get; set; }
        public double instrumentPrice { get; set; }
        //public int instrumentTypeId { get; set; }
    }
}
