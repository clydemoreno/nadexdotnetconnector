﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TransportModel;

namespace NadexDBLib
{
    public class WorkTaskStoreAllNews : WorkTask
    {
        public WorkTaskStoreAllNews()
        {
            //set new URL
            Url = "http://tradingws.kyoobiq.com:8080/NadexWS/webresources/news/all";
        }

        public override void Start()
        {
            string formattedUri = String.Format(CultureInfo.InvariantCulture, Url);
            HttpWebRequest webRequest = GetWebRequest(formattedUri);

            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
            string jsonResponse = string.Empty;
            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            {
                jsonResponse = sr.ReadToEnd();
            }
            DeserializeJson(jsonResponse);
        }

        public override void DeserializeJson(string jsonResponse)
        {
            try
            {
                returnObject = JsonConvert.DeserializeObject<List<NewsModel>>(jsonResponse);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
        }
    }
}
