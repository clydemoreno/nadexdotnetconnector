#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using NadexDBLib;
using TransportModel;
using System.Collections;
using System.Collections.Generic;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public class MyPriceUpdateIndicator : IndicatorBase
    {
        [Description("This Class is from GranbergLib.dll")]

        #region COPY THESE

        #region Variables
        private int myInput0 = 1; // Default setting for MyInput0
			public static int counter = 0;
        #endregion
		
		protected override void OnStartUp()
		{
            Print("OnStartUp MyPriceUpdateIndicator.cs     " + DateTime.Now.ToLongTimeString());
			Print(Instrument.FullName.ToString());

            Scheduler s = Scheduler.Instantiate;

			s.WorkTask.Add(new WorkTaskUpdateInstrument());

            s.Settings["Name"] = "EUR/USD"; //Instrument.FullName.ToString();
            s.Settings["Price"] = 888.88;
            s.Settings["MovingAverageInDays"] = "15";

            s.StartWorker(1);//every 1 minute
            s.PropertyChanged += s_PropertyChanged; 
		}

		public void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlePropertyChange(sender, e);
			counter++;
        }
		
 		private void HandlePropertyChange(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;

                foreach (var item in lists)
                {
					/*UPDATE PRICE*/
					Scheduler s = Scheduler.Instantiate;
                    if (item.GetType() == typeof(List<string>))
                    {
                        List<string> priceStatus = (List<string>)(item);
						Print(string.Format("counter: {0}\t Price: {1} \t {2}",counter ,s.Price, priceStatus[0].ToString()));
                    }
					
                }
            }
        }
		
        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.Orange), PlotStyle.Line, "Plot0"));
            Overlay	= false;
        }

        protected override void OnBarUpdate()
        {
            Scheduler s = Scheduler.Instantiate;
			s.Price = Close[0];
            Plot0.Set(Close[0]);
			Print(string.Format("\t\tcurrent OpenPrice is : {0}",Open[0]));
			Print(string.Format("\t\tcurrent ClosePrice is : {0}",Close[0]));
        }

        #endregion

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int MyInput0
        {
            get { return myInput0; }
            set { myInput0 = Math.Max(1, value); }
        }
        #endregion
    }
}
