﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportModel
{
    public class NewsHashValueModel
    {
        public NewsModel News { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
