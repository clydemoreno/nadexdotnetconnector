﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using TransportModel;


namespace NadexDBLib
{
    public abstract class WorkTask
    {
       
        public string Message { get; set; }

        protected string Url = "";

        public object returnObject;

        private Dictionary<string, object> settings;

        public Dictionary<string, object> Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = new Dictionary<string, object>();
                }
                return settings;
            }
            set
            {
                settings = value;

            }
        }


        //public List<object> Parameters
        //{
        //    get {
        //        if (parameters == null)
        //        {
        //            parameters = new List<object>();
        //        }
        //        return parameters;
        //    }
        //}

        public abstract void Start();

        public abstract void DeserializeJson(string jsonResponse);

        public static HttpWebRequest GetWebRequest(string formattedUri)
        {
            // Create the request’s URI.
            Uri serviceUri = new Uri(formattedUri, UriKind.Absolute);

            // Return the HttpWebRequest.
            return (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
        }

        protected void Print(string s)
        {
            Console.WriteLine(s);
        }


    }
}
