#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using System.Collections;
using System.Collections.Generic;
using NadexDBLib;
using TransportModel;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{    
    public class MyHourlyPipsIndicator : IndicatorBase
    {
        [Description("This Class is from GranbergLib.dll")]

        #region COPY THESE

        #region Variables
        // Wizard generated variables
        // User defined variables (add any user defined variables below)
        #endregion

        protected override void OnStartUp()
        {
            Print("OnStartUp MyHourlyPipsIndicator.cs     " + DateTime.Now.ToLongTimeString());

            Scheduler s = Scheduler.Instantiate;
            s.Settings["Name"] = "EUR/JPY";
            s.Settings["Price"] = 888.88;
            s.Settings["MovingAverageInDays"] = "15";

            AccessForm gridForm = AccessForm.Instantiate;
            gridForm.Open();
        }

        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.SteelBlue), PlotStyle.Bar, "Plot0"));
            Overlay = false;
        }

        protected override void OnBarUpdate()
        {
            Plot0.Set(Close[0]);
        }

        #endregion
		
        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        #endregion
    }
}
