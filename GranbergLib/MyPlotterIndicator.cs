﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NinjaTrader.Indicator;
using NadexDBLib;
using System.Collections;
using TransportModel;
using NinjaTrader.Gui.Chart;
using System.Drawing;
using System.ComponentModel;
using System.Xml.Serialization;
using NinjaTrader.Data;

namespace GranbergLib
{
    public class MyPlotterIndicator : IndicatorBase
    {
        [Description("This Class is from GranbergLib.dll")]

        #region COPY THIS

        #region Variables
        // Wizard generated variables
        // User defined variables (add any user defined variables below)
        public int counter = 0;
        public Hashtable filteredNews = new Hashtable();
        public NewsHashValueModel mediumNewsValue = new NewsHashValueModel();
        public NewsHashValueModel highNewsValue = new NewsHashValueModel();
        #endregion

        protected override void OnStartUp()
        {
            Print("OnStartUp MyPlotterIndicator.cs     " + DateTime.Now.ToLongTimeString());

            Scheduler s = Scheduler.Instantiate;

            s.WorkTask.Add(new WorkTaskGetAllNews());
            s.StartWorker();
            s.PropertyChanged += s_PropertyChanged;
        }

        protected override void Initialize()
        {
            Add(new Plot(Color.FromKnownColor(KnownColor.OrangeRed), PlotStyle.Line, "Plot0"));
            Add(new Plot(Color.FromKnownColor(KnownColor.MediumOrchid), PlotStyle.Line, "Plot1"));
            Overlay = false;
        }

        private void s_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == "ResultList") && (sender.GetType() == typeof(ArrayList)))
            {
                ArrayList lists = (ArrayList)sender;
                Scheduler s = Scheduler.Instantiate;

                foreach (var item in lists)
                {
                    if (item.GetType() == typeof(List<NewsModel>))
                    {
                        List<NewsModel> newsList = (List<NewsModel>)(item);
                        NewsCollection nc = NewsCollection.Instantiate;
                        //EDIT NAME // > to >  //Instrument.FullName.ToString();
                        filteredNews = nc.GetFilteredHashTable("EUR/USD", newsList, 10);
                    }
                }
            }
            counter++;
        }


        protected override void OnBarUpdate()
        {
            string name = "EUR/USD";
            //EDIT NAME // > to >  //Instrument.FullName.ToString();
            object mediumNewsKey = string.Format("{0}|{1}|{2}|{3}|{4}", name, Time[0].ToShortDateString(), Time[0].Hour, Time[0].Minute, "Medium");
            object highNewsKey = string.Format("{0}|{1}|{2}|{3}|{4}", name, Time[0].ToShortDateString(), Time[0].Hour, Time[0].Minute, "High");

            if (filteredNews.ContainsKey(mediumNewsKey))
            {
                mediumNewsValue = (NewsHashValueModel)filteredNews[mediumNewsKey];
                Print("  !  medium news on  "+mediumNewsKey.ToString());
            }
            if (filteredNews.ContainsKey(highNewsKey))
            {
                highNewsValue = (NewsHashValueModel)filteredNews[mediumNewsKey];
                Print("  !!  high news on  " + highNewsKey.ToString());
            }

            if ((mediumNewsValue.StartTime <= Time[-1]) && (mediumNewsValue.EndTime >= Time[0]))
            {
                Plot0.Set(Close[0]);
                Print(".... !");
            }
            if ((highNewsValue.StartTime <= Time[-1]) && (highNewsValue.EndTime >= Time[0]))
            {
                Plot1.Set(Close[0]);
                Print(".... !!");
            }


        }

        #endregion

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }

        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot1
        {
            get { return Values[1]; }
        }

        #endregion
    }
}
